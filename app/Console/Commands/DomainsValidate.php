<?php

namespace App\Console\Commands;

use App\Models\Domain;
use App\Jobs\ValidateDomain;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Bus;

class DomainsValidate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'domains:validate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Validate domain names';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function handle()
    {
        $batch = Bus::batch(
                    $this->getDomainsToValidate()      
                    )
                    ->name('Running domain ownership validations')
                    ->dispatch();

        return 0;
    }

    private function getDomainsToValidate()
    {
        $jobs = [];

        foreach(Domain::pendingValidation()->get() as $domain)
        {
            $jobs[] = new ValidateDomain($domain);
        }

        return $jobs;
    }
}
