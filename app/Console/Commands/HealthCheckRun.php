<?php

namespace App\Console\Commands;

use App\Models\Alert;
use App\Jobs\RunHealthcheck;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Bus;

class HealthCheckRun extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'healthcheck:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run healthcheck';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $batch = Bus::batch(
                    $this->getAlertDefinitions()      
                    )
                    ->name('Running healthchecks')
                    ->dispatch();

        return 0;
    }

    private function getAlertDefinitions()
    {
        $jobs = [];

        foreach(Alert::all() as $alert)
        {
            if($alert->isTimeToCheck())
                $jobs[] = new RunHealthcheck($alert);
        }

        return $jobs;
    }
}
