<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\FetchUpdatedInfoForAllDomains;

class DomainsUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'domains:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all doamins';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        FetchUpdatedInfoForAllDomains::dispatch();
        
        return 0;
    }
}
