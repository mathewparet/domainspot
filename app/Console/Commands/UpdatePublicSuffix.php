<?php

namespace App\Console\Commands;

use ReflectionClass;
use Utopia\Domains\Domain;
use App\Tools\DomainHelper;
use Illuminate\Support\Str;
use App\Models\PublicSuffix;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdatePublicSuffix extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-public-suffix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update suffix list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(DomainHelper $domain_helper)
    {
        $domain_helper->updateCache();

        return 0;
    }

}
