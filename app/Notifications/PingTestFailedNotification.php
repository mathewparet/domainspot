<?php

namespace App\Notifications;

use App\Models\Alert;
use App\Models\Domain;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PingTestFailedNotification extends Notification
{
    use Queueable;
    var $domain;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Domain $domain, Alert $alert)
    {
        $this->alert = $alert;
        $this->domain = $domain;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                                ->subject($this->domain->name.' on port '.$this->alert->port.' is down!')
                                ->greeting("Hello ".$notifiable->name.',')
                                ->line("This is to let you know that the domain ".$this->domain->name." on port ".$this->alert->port." is down and has breached the threshold of tolerated failures.")
                                ->action('Manage alerts for '.$this->domain->name, route('my-domains.alerts.index', ['my_domain'=>$this->domain->id]))
                                ->line('Thank you for using '.config('app.name'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function viaQueues()
    {
        return [
            'mail' => 'email-alerts',
        ];
    }
}
