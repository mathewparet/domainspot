<?php

namespace App\Notifications;

use App\Models\Certificate;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SSLExpiryResetNotification extends Notification
{
    use Queueable;
    var $certificate;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Certificate $certificate)
    {
        $this->certificate = $certificate;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                                ->subject('SSL certificate for '.$this->certificate->domain->name.' has been renewed')
                                ->greeting("Hello ".$notifiable->name.',')
                                ->line("We have noticed that the SSL certificate for ".$this->certificate->domain->name." has been renewed. The new certificate will expire on ".$this->certificate->expires_at->isoFormat('Do MMM, Y').".")
                                ->action('Manage alerts for '.$this->certificate->domain->name, route('my-domains.alerts.index', ['my_domain'=>$this->certificate->domain->id]))
                                ->line('Thank you for using '.config('app.name'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function viaQueues()
    {
        return [
            'mail' => 'email-alerts',
        ];
    }
}
