<?php

namespace App\Notifications;

use App\Models\Certificate;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SSLExpiryNotification extends Notification implements ShouldQueue
{
    use Queueable;

    var $certificate;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Certificate $certificate)
    {
        $this->certificate = $certificate;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                                ->subject('SSL certificate for '.$this->certificate->domain->name.' is expiring soon')
                                ->greeting("Hello ".$notifiable->name.',')
                                ->line("It is time to renew the SSL certificate for ".$this->certificate->domain->name.". It will expire on ".$this->certificate->expires_at->isoFormat('Do MMM, Y').".")
                                ->action('Manage alerts for '.$this->certificate->domain->name, route('my-domains.alerts.index', ['my_domain'=>$this->certificate->domain->id]))
                                ->line('Thank you for using '.config('app.name'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function viaQueues()
    {
        return [
            'mail' => 'email-alerts',
        ];
    }
}
