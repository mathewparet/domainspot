<?php
namespace App\HealthChecks;

use App\Models\Certificate;
use App\Notifications\SSLExpiryNotification;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SSLExpiryResetNotification;

class SSLCheck extends HealthCheck
{
    protected $description = 'Check SSL certificate expiry';

    protected $is_expiry_check = true;

    public function isValid()
    {
        if(now()->setTimezone($this->alert->user->timezone)->hour >= 9)
            return $this->alert->alertable->expires_at
                && $this->alert->alertable->expires_at->diffInDays(now()) >= $this->alert->tolerance;
        else
            return true;
    }

    public function sendFailureAlert()
    {
        return SSLExpiryNotification::class;
    }
    
    public function sendSuccessAlert()
    {
        return SSLExpiryResetNotification::class;
    }

    public function isValidFor($object)
    {
        return $object instanceof Certificate;
    }
}