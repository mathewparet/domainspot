<?php
namespace App\HealthChecks;

use App\Models\Domain;
use Illuminate\Support\Facades\Notification;
use App\Notifications\RegistrationExpiryNotification;
use App\Notifications\RegistrationExpiryResetNotification;

class DomainRegistrationCheck extends HealthCheck
{
    protected $description = 'Check Domain Registration Expiry';

    protected $is_expiry_check = true;

    public function isValid()
    {
        if($this->alert->alertable->is_subdomain)
            return true;

        if(now()->setTimezone($this->alert->user->timezone)->hour >= 9)
        {
            if($this->alert->alertable->whois()->exists())
                return $this->alert->alertable->whois->expires_at
                    && $this->alert->alertable->whois->expires_at->diffInDays(now()) >= $this->alert->tolerance;
            else
                return true;
        }
        else
            return true;
    }

    public function sendFailureAlert()
    {
        return RegistrationExpiryNotification::class;
    }
    
    public function sendSuccessAlert()
    {
        return RegistrationExpiryResetNotification::class;
    }

    public function isValidFor($object)
    {
        return $object instanceof Domain;
    }
}