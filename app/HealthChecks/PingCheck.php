<?php
namespace App\HealthChecks;

use App\Models\Domain;
use App\Notifications\PingTestResetNotification;
use App\Notifications\PingTestFailedNotification;

class PingCheck extends HealthCheck
{
    protected $description = 'Ping server to check if server is responding';

    public function isValid()
    {
        $site_is_up = null;

        $error_code = $error_messag = null;

        $starttime = microtime(true);
        $connection = @fsockopen($this->alert->alertable->name,$this->alert->port, $error_code, $error_messag, 1);
        $stoptime  = microtime(true);
        
        if($connection)
        {
            $site_is_up = true;

            if(in_array($this->alert->port, [80, 443]))
                $this->alert->alertable->addPing(($stoptime - $starttime)/1,000,000);
            
                $this->alert->alertable()->first()->markAsUp();
            @fclose($connection);
        }
        else
        {
            $site_is_up = false;
            $this->alert->alertable()->first()->markAsDown();
        }


        return $site_is_up;
    }

    public function sendFailureAlert()
    {
        return PingTestFailedNotification::class;
    }
    
    public function sendSuccessAlert()
    {
        return PingTestResetNotification::class;
    }

    public function isValidFor($object)
    {
        return $object instanceof Domain;
    }
}