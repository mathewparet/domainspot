<?php
namespace App\HealthChecks;

use App\Models\Alert;
use Illuminate\Support\Str;
use App\Exceptions\InvalidCheckpointDefinition;

class HealthCheck
{
    protected $config;
    protected $is_valid;
    protected $alert;
    protected $is_expiry_check = false;

    public function __construct($config = null)
    {
        if($config)
            $this->config = $config;
    }

    public function getDriver($provider)
    {
        $driver = null;

        try
        {
            $driver = $this->config[$provider]['driver'];
        }
        catch(\Exception $e)
        {
            throw new InvalidCheckpointDefinition("No healthcheck definition found for $provider");
        }

        return new $driver($this->config[$provider]);
    }

    public function sendFailureAlert()
    {
        throw new InvalidCheckpointDefinition("Missing sendFailureAlert() method in ".\get_called_class());
    }
    
    public function sendSuccessAlert()
    {
        throw new InvalidCheckpointDefinition("Missing sendSuccessAlert() method in ".\get_called_class());
    }

    public function isValid()
    {
        throw new InvalidCheckpointDefinition("Missing isValid() method in ".\get_called_class());
    }

    private function executeAlertValidation()
    {
        if(app()->call([$this, "isValid"]))
            $this->alert->markSuccess();
        else
            $this->alert->markFailed();
    }

    private function notifySucces()
    {
        if( ( !$this->is_expiry_check && $this->alert->needsToAlertSuccess()) || $this->needsToAlertSuccess() )
        {
            $this->generateNotification("sendSuccessAlert");
            $this->alert->resetAlert();
        }
    }

    private function generateNotification($method)
    {
        $notification = app()->call([$this, $method]);

        if(is_string($notification))
            $this->alert->user->notify(new $notification($this->alert->alertable, $this->alert));
    }

    private function notifyFailure()
    {
        if( ( !$this->is_expiry_check && $this->alert->needsToAlertFailure()) || $this->needsToAlertExpiry() )
        {
            $this->generateNotification("sendFailureAlert");
            $this->alert->markAsAlerted();
        }
    }

    private function needsToAlertExpiry()
    {
        return $this->is_expiry_check && $this->alert->failures && !$this->alert->userHasBeenAlerted();
    }
    
    private function needsToAlertSuccess()
    {
        return $this->is_expiry_check && !$this->alert->failures && $this->alert->userHasBeenAlerted();
    }

    private function sendAlerts()
    {
        $this->notifyFailure();

        $this->notifySucces();   
    }
    
    public function validate(Alert $alert)
    {        
        $this->alert = $alert;

        $this->executeAlertValidation();
        
        $this->sendAlerts();
    }

    public function __get($property)
    {
        return $this->$property ? : null;
    }

    public function isValidFor($object)
    {
        throw new InvalidCheckpointDefinition("Missing isValidFor() method in ".\get_called_class());
    }

}