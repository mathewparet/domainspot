<?php
namespace App\Tools;

use ReflectionClass;
use Utopia\Domains\Domain;
use Illuminate\Support\Str;

class DomainHelper
{
    public function isRegisterable($domain)
    {
        $domain = Str::lower($domain);
        $utopia = new Domain($domain);
        
        return $utopia->getRegisterable() === $domain;
    }

    public function updateCache()
    {
        require_once($this->getImporterPath());
    }

    private function getImporterPath()
    {
        $vendor_path = (new ReflectionClass(Domain::class))->getFilename();
        $vendor_base_dir = Str::finish(dirname($vendor_path), "/");

        return realpath($vendor_base_dir.'../../data/import.php');
    }
}