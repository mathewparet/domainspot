<?php

namespace App\Jobs;

use App\Models\Domain;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Spatie\SslCertificate\Exceptions\CouldNotDownloadCertificate\UnknownError;

class UpdateSSLDetails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Batchable;

    private $domain, $batch_mode;
    public $deleteWhenMissingModels = true;
    private $alerts;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * The maximum number of unhandled exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * Indicate if the job should be marked as failed on timeout.
     *
     * @var bool
     */
    public $failOnTimeout = true;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Domain $domain, $batch_mode = true)
    {
        $this->domain = $domain;
        $this->batch_mode = $batch_mode;
    }

    public function tags()
    {
        return ['domain:'.$this->domain->name];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->batch_mode && $this->batch()->cancelled()) {
            return;
        }
        
        try
        { 
            $certificate = $this->domain->updateSSLDetails();
        }
        catch(UnknownError $e)
        {
            Log::error("Couldn't fetch SSL Certificate: ", ['domain'=>$this->domain->name, 'error'=>$e->getMessage()]);

            if($this->attempts() >= $this->tries)
            {
                Log::error("Max tries reached. SSL fetch will not be attempted again", ['domain' => $this->domain->name]);
                $this->domain->markSSLPullFailed();
                $this->fail();
            }

            $this->release($this->attempts() * 60);
        }
    }
}
