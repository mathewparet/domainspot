<?php

namespace App\Jobs;

use App\Models\Alert;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use App\HealthChecks\HealthCheck;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class RunHealthcheck implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Batchable;
    var $alert = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Alert $alert)
    {
        $this->alert = $alert;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(HealthCheck $health_check)
    {
        if ($this->batch()->cancelled()) {
            return;
        }
        
        $health_check->getDriver($this->alert->type)->validate($this->alert);
    }

}
