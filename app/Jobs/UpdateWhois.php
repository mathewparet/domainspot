<?php

namespace App\Jobs;

use App\Models\Domain;
use App\Models\Issuer;
use App\Models\Registrar;
use App\Models\Certificate;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Iodev\Whois\Factory as WhoisFactory;
use Spatie\SslCertificate\SslCertificate;
use Iodev\Whois\Exceptions\WhoisException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Iodev\Whois\Exceptions\ConnectionException;
use Iodev\Whois\Exceptions\ServerMismatchException;
use Illuminate\Bus\Batchable;

class UpdateWhois implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Batchable;

    private $domain, $batch_mode;
    public $deleteWhenMissingModels = true;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 25;

    /**
     * The maximum number of unhandled exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * Indicate if the job should be marked as failed on timeout.
     *
     * @var bool
     */
    public $failOnTimeout = true;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Domain $domain, $batch_mode = true)
    {
        $this->domain = $domain;
        $this->batch_mode = $batch_mode;
    }

    public function tags()
    {
        return ['domain:'.$this->domain->name];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->batch_mode && $this->batch()->cancelled()) {
            return;
        }
        
        try
        { 
            $this->domain->updateWhoIs();
        }
        catch (ServerMismatchException $e) 
        {
            Log::error("TLD server for domain not found in current server hosts", [
                'domain' => $this->domain->name,
                'error' => $e->getMessage()
            ]);

            $this->fail();
        }
        catch(WhoisException $e)
        {
            Log::error("Error fetching whois information", [
                'domain' => $this->domain->name,
                'error' => $e->getMessage()
            ]);

            if(Str::contains($e->getMessage(), "WHOIS LIMIT EXCEEDED"))
            {
                $retry_after = $this->attempts() * 60 * 10;

                Log::debug("Will retry after ".$retry_after." seconds");
                
                $this->release($retry_after);
            }
            else
                $this->fail();
        }
        catch (ConnectionException $e) 
        {
            Log::error("Disconnect or connection timeout", [
                'domain' => $this->domain->name,
                'error' => $e->getMessage()
            ]);

            $retry_after = $this->attempts() * 60;

            Log::debug("Will retry after ".$retry_after." seconds");
            
            $this->release($retry_after);
        }
        catch(\Exception $e)
        {
            Log::error("Error fetching WhoIs", ['domain' => $this->domain]);
            $this->fail();
        }
    }
}
