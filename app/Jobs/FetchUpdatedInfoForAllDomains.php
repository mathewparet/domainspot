<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Bus\Batch;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Domain;
use Illuminate\Support\Facades\Bus;

class FetchUpdatedInfoForAllDomains implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $batch = Bus::batch(
                    $this->getJobs()      
                )
                ->name('Updating domains')
                ->dispatch();
    }

    private function getJobs()
    {
        $jobs = [];

        foreach(Domain::get() as $domain)
        {
            if($domain->is_registerable)
                $jobs[] = new UpdateWhois($domain);

            $jobs[] = new UpdateFavicon($domain);
            
            $jobs[] = new UpdateSSLDetails($domain);
        }

        return $jobs;
    }
}
