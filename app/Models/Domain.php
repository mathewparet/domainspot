<?php

namespace App\Models;

use DOMDocument;
use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Support\Str;
use App\Tools\FaviconDownloader;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Notifications\DomainNameSuccessfullyVerified;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Domain extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    protected $appends = ['latest_response_time', 'total_down_time', 'average_response_time', 'verified', 'user_verification_key', 'is_registerable'];

    public function getTotalDownTimeAttribute()
    {
        return Cache::remember("domain_down_30:".$this->id, 30, function() {
            return $this->downtimes()->whereDate("created_at", ">", now()->subDays(30))->sum('duration');
        });
    }

    public function generateValidationKey()
    {
        Cache::remember("domain_verification:".$this->id, 60*60, function() {
            return Str::random(64);
        });

        return Cache::get("domain_verification:".$this->id);
    }

    public function saveValidationKey()
    {
        $this->verification_key = [
            'user' => auth()->user()->id,
            'key' => Cache::pull("domain_verification:".$this->id)
        ];
        $this->save();
    }

    public function getUserVerificationKeyAttribute()
    {
        $vkey = $this->parseVerificationKey();
        
        if(auth()->user())
            return $vkey && $vkey['user'] == auth()->user()->id
                ? $vkey['key']
                : null;
        else
            return $vkey ? $vkey['key'] : null;
    }

    public function getVerificationKeyAttribute()
    {
        $vkey = $this->parseVerificationKey();
        return $vkey ? $vkey['key'] : null;
    }

    private function parseVerificationKey()
    {
        if(blank($this->attributes['verification_key']))
            return null;
        
        return Crypt::decrypt($this->attributes['verification_key']);
    }

    public function setVerificationKeyAttribute($value)
    {
        $this->attributes['verification_key'] = Crypt::encrypt($value);
    }

    public function updateWhoIs()
    {
        if($this->whois)
            return $this->whois->updateFromExternal();
        else
            return Whois::createFromExternal($this);
    }

    public function updateSSLDetails()
    {
        if($this->certificate)
            return $this->certificate->updateFromExternal();
        else
            return $this->certificate()->save(new Certificate($this->name));
    }

    public function markSSLPullFailed()
    {
        if(!$this->certificate)
            $this->certificate()->save(new Certificate());
    }

    public function getFaviconAttribute()
    {
        if(!blank($this->attributes['favicon']))
            return Storage::url($this->attributes['favicon']);
        else
            return null;
    }

    public function whois()
    {
        return $this->belongsTo(Whois::class);
    }

    public function scopeRegisterable($query)
    {
        return $query->where('is_subdomain', false)->orWhereNull('is_subdomain');
    }

    public function getIsRegisterableAttribute()
    {
        return ! $this->is_subdomain;
    }
    
    public function setIsRegisterableAttribute($value)
    {
        return $this->attributes['is_subdomain'] = !$value;
    }

    public function fetchFavicon()
    {
        $old_favicon = null;

        $temp_file = \tempnam(sys_get_temp_dir(), "favicon");
        $icon = new FaviconDownloader("http://$this->name");

        if(!$icon->icoExists)
        {
            copy("https://ui-avatars.com/api/?name=".$this->name, $temp_file);
        }
        else
            \file_put_contents($temp_file, $icon->icoData);
        
        if($this->favicon)
            $old_favicon = $this->getRawOriginal('favicon');
    
        $this->favicon = Storage::putFile('favicons', new File($temp_file));

        $this->save();

        if($old_favicon)
            Storage::delete($old_favicon);
    }


    public function setNameAttribute($value)
    {
        $this->attributes['name'] = Str::lower($value);
    }

    public function certificate()
    {
        return $this->hasOne(Certificate::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function cleanup()
    {
        if(!$this->refresh()->users()->count())
        {
            if($this->certificate()->exists())
            {
                if($this->certificate()->first()->alerts()->exists())
                    $this->certificate()->first()->alerts()->whereUserId(request()->user()->id)->delete();
                
                $this->certificate()->delete();
            }
            
            Storage::delete($this->getRawOriginal('favicon'));
            $this->delete();

            if($this->whois()->exists())
                $this->whois()->first()->delete();
        }
    }

    public function scopePendingValidation($query)
    {
        return $query->whereNotNull('verification_key')->whereNull('user_id');
    }

    public function validateOwnership()
    {
        Log::debug("Validting domain ownership", ['domain'=>$this->name]);

        if($this->getVerificationRecordFromDNS())
        {
            $this->user_id = Crypt::decrypt($this->attributes['verification_key'])['user'];
            $this->save();
            $this->fresh();
            $this->owner->notify(new DomainNameSuccessfullyVerified($this));
        }
    }

    private function getVerificationRecordFromDNS()
    {
        $records = dns_get_record($this->name, DNS_TXT);

        foreach($records as $record)
        {
            if($record['host'] == $this->name)
            {
                Log::debug("DNS Record", [
                    'dns'=>$record['txt'],
                    'db'=>"domainspot-net-verification=".$this->verification_key
                ]);
                
                if($record['txt'] == "domainspot-net-verification=".$this->verification_key)
                    return true;
            }
        }

        return false;
    }

    public function getVerifiedAttribute()
    {
        if(!auth()->user())
            return null;

        return optional($this->owner)->id === auth()->user()->id;
    }

    public function owner()
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function pings()
    {
        return $this->hasMany(Ping::class);
    }

    public function getLatestResponseTimeAttribute()
    {
            if(Cache::remember("pings_exists:".$this->id, 30, function() {
                return $this->pings()->count() > 0;
            }))
            {
                return Cache::remember("pings_latest:".$this->id, 30, function() {
                    return round($this->pings()->latest()->first()->response, 4);
                });
            }
            else
                return  null;
    }

    public function getAverageResponseTimeAttribute()
    {
            if(Cache::remember("pings_exists:".$this->id, 30, function() {
                return $this->pings()->count() > 0;
            }))
            {
                return Cache::remember("pings_avg:".$this->id, 30, function() {
                    return round($this->pings()->whereDate("created_at", ">", now()->subDays(30))->avg('response'), 4);
                });
            }
            else
                return  null;
    }

    public function addPing($response)
    {
        return $this->pings()->save(new Ping(compact('response')));
    }

    public function downtimes()
    {
        return $this->hasMany(Downtime::class);
    }

    public function markAsDown()
    {
        $current_downtime = $this->downtimes()->current()->first();
        
        if(!$current_downtime)
            return $this->downtimes()->save(Downtime::markStarted());
    }

    public function markAsUp()
    {
        $current_downtime = $this->downtimes()->current()->first();

        if($current_downtime)
            $current_downtime->markEnded();
    }

    public function alerts()
    {
        return $this->morphMany(Alert::class, "alertable");
    }

    public function createAlert(Alert $alert)
    {
        return $this->alerts()->save($alert);
    }

}
