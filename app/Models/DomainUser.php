<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class DomainUser extends Pivot
{
    use HasFactory;

    public $incrementing = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function domain()
    {
        return $this->hasOne(Domain::class);
    }
}
