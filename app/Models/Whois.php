<?php

namespace App\Models;

use Exception;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Iodev\Whois\Factory as WhoisFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Whois extends Model
{
    use HasFactory;

    protected $dates = ['last_checked_at', 'expires_at'];

    public function domain()
    {
        return $this->hasOne(Domain::class);
    }

    public function registrar()
    {
        return $this->belongsTo(Registrar::class);
    }

    public function updateFromExternal()
    {   
        $this->updateWhoisModelFromExternal($this->domain->name);

        return $this;
    }

    public static function createFromExternal(Domain $domain)
    {
        $whois = new Self();
        $whois->updateWhoisModelFromExternal($domain->name);
        $domain->whois_id = $whois->id;
        $domain->save();
        return $whois->refresh();
    }

    private function updateWhoisModelFromExternal($name)
    {
        $domain_whois_info = $this->queryWhoIsServer($name);

        $this->registrar_id = $this->fetchOrCreateRegistrar($domain_whois_info)->id;

        if($domain_whois_info->expirationDate)
            $this->expires_at = Carbon::createFromTimeStamp($domain_whois_info->expirationDate);

        $this->whois = $domain_whois_info->getResponse()->text;

        $this->save();
    }

    private function queryWhoIsServer($name)
    {
        $whois_engine = WhoisFactory::get()->createWhois();
        return $whois_engine->loadDomainInfo($name);
    }

    public function fetchOrCreateRegistrar($domain_info)
    {
        try
        {
            $registrar_record = Registrar::whereName($domain_info->registrar)->first();

            if($registrar_record)
                return $registrar_record;

            $registrar_record = new Registrar([
                'name' => $domain_info->registrar, 
            ]);

            $groups = $domain_info->getExtraVal('groups');

            if( isset($groups[0]) && isset($groups[0]['Registrar URL']) )
                $registrar_record->url = $domain_info->getExtraVal('groups')[0]['Registrar URL'];

            $registrar_record->save();

            return $registrar_record;
        }
        catch(\Exception $e)
        {
            Log::error("Error parsing whois", [
                'error' => $e->getMessage(),
                'whois' => $this->domain_info
            ]);

            throw $e;
        }
    }

}
