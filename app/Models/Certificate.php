<?php

namespace App\Models;

use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Spatie\SslCertificate\SslCertificate;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\SslCertificate\Exceptions\CouldNotDownloadCertificate\HostDoesNotExist;
use Spatie\SslCertificate\Exceptions\CouldNotDownloadCertificate\NoCertificateInstalled;

class Certificate extends Model
{
    use HasFactory;

    protected $fillable = ['expires_at'];

    protected $casts = [
        'expires_at' => 'datetime'
    ];

    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }

    public function alerts()
    {
        return $this->morphMany(Alert::class, "alertable");
    }

    public function issuer()
    {
        return $this->belongsTo(Issuer::class);
    }

    public function updateFromExternal()
    {
        $this->updateCertificateModelFromExternal($this->domain->name);

        $this->save();

        return $this;
    }

    public function __construct($name = null)
    {
        if(!$name)
            return null;

        $this->updateCertificateModelFromExternal($name);

        return $this;
        
    }

    private function updateCertificateModelFromExternal($name)
    {
        try
        {
            $certificate = null;
            
            $certificate = SslCertificate::createForHostName($name);

            $issuer = $this->fetchOrCreateIssuer($certificate);

            $this->expires_at = $certificate->expirationDate()->timestamp;
            
            $this->issuer_id = $issuer->id;

            $this->certificate = $certificate;
        }
        catch(HostDoesNotExist $e)
        {

        }
        catch(NoCertificateInstalled $e)
        {

        }
    }

    private function fetchOrCreateIssuer($certificate)
    {
        $cert_json = json_decode($certificate);
        
        $issuer = Issuer::whereName($cert_json->issuer->O)->first();
        
        if($issuer)
            return $issuer;

        $issuer = new Issuer(['name'=>$cert_json->issuer->O]);
        
        try
        {
            if( $cert_json->issuer && isset($cert_json->issuer->OU) && !is_array($cert_json->issuer->OU))
                $issuer->url = $cert_json->issuer->OU;
        }
        catch(\Exception $e)
        {
            Log::error("Certificate error", compact('cert_json'));
        }
        
        $issuer->save();

        return $issuer;
    }

    public function createAlert(Alert $alert)
    {
        return $this->alerts()->save($alert);
    }

}
