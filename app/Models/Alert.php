<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    use HasFactory;

    protected $fillable = ['type', 'frequency', 'tolerance', 'port'];

    protected $hidden = ['alertable_type', 'alertable_id'];

    protected $dates = ['last_checked'];

    private $failures_before_success = null;

    public $recently_success = false;

    public function markFailed()
    {
        $this->failures++;
        $this->last_checked = now();
        $this->save();
    }

    public function markAsAlerted($alerted = true)
    {
        $this->alerted = $alerted;
        $this->save();
    }

    public function breachedFailureThreshold()
    {
        return $this->failures >= $this->tolerance;
    }

    public function userHasBeenAlerted()
    {
        return $this->alerted;
    }

    public function userHasNotBeenAlerted()
    {
        return !$this->userHasBeenAlerted();
    }

    public function needsToAlertFailure()
    {
        return $this->breachedFailureThreshold() && $this->userHasNotBeenAlerted();
    }
    
    public function needsToAlertSuccess()
    {
        return $this->recently_success && $this->failures_before_success >= $this->tolerance;
    }

    public function resetAlert()
    {
        $this->markAsAlerted(false);
    }

    public function markSuccess()
    {
        if($this->failures > 0)
        {
            $this->recently_success = true;
            $this->failures_before_success = $this->failures;
        }

        $this->failures = 0;
        $this->last_checked = now();
        $this->save();
    }

    public function isTimeToCheck()
    {
        if($this->last_checked)
            return $this->last_checked->add($this->frequency) <= now()->addSeconds(15);
        else
            return true;
    }

    public function alertable()
    {
        return $this->morphTo();
    }

    public static function getProfile($type)
    {
        return config('healthcheck')[$type];
    }

    public static function supportsModel($type, $model)
    {
        return self::getProfile($type)['model'] == get_class($model);
    }

    public function forUser($query, User $user)
    {
        return $query->where('user_id', $user->id);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
