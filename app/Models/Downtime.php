<?php

namespace App\Models;

use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Downtime extends Model
{
    use HasFactory;

    protected $fillable = ['to', 'from'];

    protected $dates = ['from', 'to'];

    protected $appends = ['duration_for_humans'];

    public function scopeCurrent($query)
    {
        return $query->whereNull('to');
    }

    public static function markStarted()
    {
        return new Self(['from' => now()]);
    }

    public function markEnded()
    {
        $this->update(['to' => now()]);
        return $this->fresh();
    }

    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }

    public function getDurationForHumansAttribute()
    {
        return now()->subSeconds($this->attributes['duration'])->diffForHumans(['parts'=>3, 'syntax' => CarbonInterface::DIFF_ABSOLUTE]);
    }
}
