<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ping extends Model
{
    use HasFactory;

    protected $fillable = ['response'];

    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }
}
