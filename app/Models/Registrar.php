<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Registrar extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'url'];

    public function whois()
    {
        return $this->hasMany(Whois::class);
    }

    public function domains()
    {
        return $this->hasManyThrough(Domain::class, Whois::class);
    }

}
