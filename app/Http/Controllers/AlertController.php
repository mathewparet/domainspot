<?php

namespace App\Http\Controllers;

use App\Models\Alert;
use App\Models\Domain;
use Illuminate\Http\Request;

class AlertController extends JetstreamController
{
    public function destroy(Request $request, Domain $my_domain, Alert $alert)
    {
        $alert->delete();

        return compact('alert');
    }

    public function update(Request $request, Domain $my_domain, Alert $alert)
    {
        $alert->update($request->only(['frequency', 'port', 'tolerance']));

        $alert->refresh();

        return compact('alert');
    }
    
    public function store(Request $request, Domain $my_domain)
    {
        $alert = new Alert($request->only(['type', 'frequency', 'tolerance', 'port']));
        $alert->frequency = $alert->frequency ? : Alert::getProfile($alert->type)['frequency'][0];
        
        if(isset(Alert::getProfile($alert->type)['port']))
            $alert->port = $alert->port ? : Alert::getProfile($alert->type)['port'][0];
        
        $alert->user_id = $request->user()->id;

        if(Alert::supportsModel($alert->type, $my_domain))
            $alert = $my_domain->createAlert($alert);
        elseif(Alert::supportsModel($alert->type, $my_domain->certificate()->first()))
            $alert = $my_domain->certificate()->first()->createAlert($alert);

        return $alert;
    }

    public function index(Domain $my_domain, Request $request)
    {
        $domain_alerts = $my_domain->alerts()->whereUserId($request->user()->id)->get()->all();
        
        $ssl_alerts = [];

        if($my_domain->certificate()->exists())
            $ssl_alerts = $my_domain->certificate()->first()->alerts()->whereUserId($request->user()->id)->get()->all();

        $alerts = array_merge($domain_alerts, $ssl_alerts);

        return $this->render($request, "Alerts/Index", [
            'alerts' => $alerts,
            'domain' => $my_domain,
            'healthcheck' => self::getConfigurableAlertsPublic()
        ]);
    }

    public static function getConfigurableAlertsPublic()
    {
        $health_check_profiles = config("healthcheck");

        foreach($health_check_profiles as &$profile)
        {
            unset($profile['model']);
            unset($profile['driver']);
        }
        
        return $health_check_profiles;
    }
}
