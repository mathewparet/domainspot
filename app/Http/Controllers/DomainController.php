<?php

namespace App\Http\Controllers;

use App\Models\Alert;
use App\Models\Domain;
use App\Jobs\UpdateWhois;
use App\Rules\isValidFDQN;
use App\Jobs\UpdateFavicon;
use App\Tools\DomainHelper;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Jobs\UpdateSSLDetails;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Rules\DomainAlreadyAttached;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class DomainController extends JetstreamController
{
    public function registerable(Request $request, DomainHelper $domain_helper)
    {
        return ['is_registerable'=> $domain_helper->isRegisterable($request->name)];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->user()->domains()->count() == 0)
            return redirect()->route('my-domains.create');

        $domains = $request->user()->domains()->with(['certificate', 'whois', 'certificate.issuer', 'whois.registrar'])->orderBy('name')->paginate(config('app.page_size'));
        
        return $this->render($request, 'Domains/Index', compact('domains'));
    }

    public function verify(Request $request, Domain $my_domain)
    {
        return $this->render($request, "Domains/Verify", [
            'domain' => $my_domain,
        ]);
    }
    
    public function verifyStart(Request $request, Domain $my_domain)
    {
        return $my_domain->generateValidationKey();
    }

    public function verifyConfirm(Request $request, Domain $my_domain)
    {
        $my_domain->saveValidationKey();
        $my_domain->fresh();

        return ['domain' => $my_domain];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return $this->render($request, 'Domains/Add', ['healthcheck' => AlertController::getConfigurableAlertsPublic()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, DomainHelper $domain_helper)
    {
        $this->validateDomain($request);
        
        $domain = $this->addOrGetDomain($request->only('name'));

        if($domain->wasRecentlyCreated === true)
            $this->dispatchJobs($domain);

        $domain->save();

        return redirect()->route("my-domains.index")->with("message", "Domain ".$domain->name." successfully added.");
    }

    private function dispatchJobs($domain)
    {
        if( $domain->is_registerable )
            UpdateWhois::dispatch($domain, false);

        UpdateSSLDetails::dispatch($domain, false);
        UpdateFavicon::dispatch($domain, false);
    }

    private function addOrGetDomain($name)
    {
        $domain = Domain::firstOrCreate($name);
        request()->user()->domains()->attach($domain);

        $domain_helper = new DomainHelper();

        $domain->is_registerable = $domain_helper->isRegisterable($domain->name);

        return $domain;
    }

    private function validateDomain(Request $request)
    {
        $type = $request->input('type');

        $request->validate([
            'name' => ['required', new isValidFDQN, new DomainAlreadyAttached(request()->user())],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function destroy(Domain $my_domain, Request $request)
    {
        DB::transaction(function() use($request, $my_domain)
        {
            $request->user()->domains()->detach($my_domain);

            $my_domain->alerts()->whereUserId($request->user()->id)->delete();
            
            if($my_domain->certificate()->exists() && $my_domain->certificate()->first()->alerts()->exists())
                $my_domain->certificate()->first()->alerts()->whereUserId($request->user()->id)->delete();

            $my_domain->cleanup();
        });

        return response()->json(['status' => "Domain ".$my_domain->name.' successfully deleted.']);
    }
}
