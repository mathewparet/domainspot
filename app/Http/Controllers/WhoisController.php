<?php

namespace App\Http\Controllers;

use App\Models\Whois;
use Illuminate\Http\Request;

class WhoisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Whois  $whois
     * @return \Illuminate\Http\Response
     */
    public function show(Whois $whois)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Whois  $whois
     * @return \Illuminate\Http\Response
     */
    public function edit(Whois $whois)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Whois  $whois
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Whois $whois)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Whois  $whois
     * @return \Illuminate\Http\Response
     */
    public function destroy(Whois $whois)
    {
        //
    }
}
