<?php

namespace App\Http\Controllers;

use Laravel\Jetstream\Jetstream;
use Illuminate\Http\Request;

class JetstreamController extends Controller
{
    protected function render(Request $request, $view, $data = [])
    {
        return Jetstream::inertia()->render($request, $view, $data);
    }

    protected function partial(Request $request, $view, $data = [])
    {
        return Jetstream::inertia()->partial($request, $view, $data);
    }
}