<?php

use App\Models\Domain;
use App\Models\Certificate;
use App\HealthChecks\SSLCheck;
use App\HealthChecks\PingCheck;
use App\HealthChecks\DomainRegistrationCheck;

return [

    'Domain_Registration_Expiry' => [
        'driver' => DomainRegistrationCheck::class,
        'model' => Domain::class,
        'default' => true,
        'needs_verification' => false,
        'needs_registerable_domain' => true,
        'frequency' => ['24 hours'],
        'tolerance' => [
            '30 days before' => 30,
            '15 days before' => 15,
            '7 days before' => 7,
            'on expiry' => -1,
        ],
        'unique' => ['frequency', 'tolerance'],
    ],
    
    'SSL_Expiry' => [
        'driver' => SSLCheck::class,
        'model' => Certificate::class,
        'needs_registerable_domain' => false,
        'needs_verification' => false,
        'frequency' => ['24 hours'],
        'default' => true,
        'tolerance' => [
            '30 days before' => 30,
            '15 days before' => 15,
            '7 days before' => 7,
            'on expiry' => -1,
        ],
        'port'=>[443],
        'unique' => ['frequency', 'tolerance', 'port'],
    ],

    'Webserver' => [
        'driver' => PingCheck::class,
        'needs_registerable_domain' => false,
        'model' => Domain::class,
        'needs_verification' => false,
        'frequency' => [
            '30 seconds',
            '1 minute',
            '3 minutes',
            '5 minutes',
        ],
        'tolerance' => [
            'immediately' => 1,
            'on 3 failures' => 3,
            'on 5 failures' => 5,
        ],
        'port' => [80, 443],
        'unique' => ['port'],
    ],
    
    'SMTP_Server' => [
        'driver' => PingCheck::class,
        'needs_registerable_domain' => false,
        'needs_verification' => true,
        'model' => Domain::class,
        'frequency' => [
            '30 seconds',
            '1 minute',
            '3 minutes',
            '5 minutes',
        ],
        'tolerance' => [
            'immediately' => 1,
            'on 3 failures' => 3,
            'on 5 failures' => 5,
        ],
        'port' => [25, 465, 587],
        'unique' => ['port'],
    ],
    
    'FTP_or_SFTP_Server' => [
        'driver' => PingCheck::class,
        'needs_registerable_domain' => false,
        'needs_verification' => true,
        'model' => Domain::class,
        'frequency' => [
            '30 seconds',
            '1 minute',
            '3 minutes',
            '5 minutes',
        ],
        'tolerance' => [
            'immediately' => 1,
            'on 3 failures' => 3,
            'on 5 failures' => 5,
        ],
        'port' => [21, 22],
        'unique' => ['port'],
    ],

    'Custom_Port' => [
        'driver' => PingCheck::class,
        'needs_registerable_domain' => false,
        'model' => Domain::class,
        'needs_verification' => true,
        'frequency' => [
            '30 seconds',
            '1 minute',
            '3 minutes',
            '5 minutes',
        ],
        'tolerance' => [
            'immediately' => 1,
            'on 3 failures' => 3,
            'on 5 failures' => 5,
        ],
        'port' => [],
        'unique' => ['port'],
    ],
    
];