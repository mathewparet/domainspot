<?php

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;
use App\Http\Controllers\AlertController;
use App\Http\Controllers\DomainController;
use App\Http\Controllers\PrivacyPolicyController;
use App\Http\Controllers\TermsOfServiceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get("privacy-policy", [PrivacyPolicyController::class, "show"])->name("policy.show");
Route::get("terms-of-service", [TermsOfServiceController::class, "show"])->name("terms.show");

Route::group(['middleware' => ['auth:sanctum', 'verified']], function() {
    
    Route::resource('my-domains', DomainController::class)->only(['create', 'index', 'destroy', 'store'])->name('*', 'my-domains');
    Route::post('is-registerable', [DomainController::class, "registerable"])->name("is-registerable");

    Route::group(['prefix'=>'my-domains/{my_domain}', 'name'=>'my-domains.'], function() {
        Route::resource("alerts", AlertController::class, ['as'=>'my-domains'])->only(['destroy', 'index', 'update', 'store']);
        Route::get("verify", [DomainController::class, "verify"])->name('my-domains.verify');
        Route::get("verifyStart", [DomainController::class, "verifyStart"])->name('my-domains.verifyStart');
        Route::post("verifyStart", [DomainController::class, "verifyConfirm"])->name('my-domains.verifyConfirm');
    });

});

