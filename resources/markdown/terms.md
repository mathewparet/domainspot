# WEBSITE TERMS AND CONDITIONS

Please take the time to read these terms and conditions.  By using Our Website and the Services and information offered on Our Website, you are agreeing to these terms and conditions.

If you purchase products through our Website, there will be additional terms and conditions relating to the purchase.  Please make sure you agree with these terms and conditions, which you will be directed to read prior to making your purchase. If not you can find more details regarding this at the Payment Processing and Refunds section.

## Definitions

**Services** means “website uptime check” provided by DomainSpot.net

**the Website** means the website https://www.domainspot.net 

**We / Us** etc means DomainSpot.Net and any subsidiaries, affiliates, employees, officers, agents or assigns.

## Accuracy of content

We have taken proper care and precautions to ensure that the information we provide on this Website is accurate.  However, we cannot guarantee, nor do we accept any legal liability arising from or connected to, the accuracy, reliability, currency or completeness of anything contained on this Website or on any linked site.

The information contained on this Website should not take the place of professional advice.  

## Use

The Website is made available for your use on your acceptance and compliance with these terms and conditions.  By using this Website, you are agreeing to these terms and conditions.

You agree that you will use this website in accordance with all applicable local, state, national and international laws, rules and regulations.

You agree that you will not use, nor will you allow or authorise any third party to use, the Website for any purpose that is unlawful, defamatory, harassing, abusive, fraudulent or obscene way or in any other inappropriate way or in a way which conflicts with the Website or the Services.  

If you contribute to our forum (if any) or make any public comments on this Website which are, in our opinion, unlawful, defamatory, harassing, abusive, fraudulent or obscene or in any other way inappropriate or which conflict with the Website or the Services offered, then we may at our discretion, refuse to publish such comments and/or remove them from the Website.

We reserve the right to refuse or terminate service to anyone at any time without notice or reason.

## Passwords and logins

You are responsible for maintaining the confidentiality of your passwords and login details and for all activities carried out under your password and login.

## Indemnification for loss or damage

You agree to indemnify Us and hold Us harmless from and against any and all liabilities or expenses arising from or in any way related to your use of this Website or the Services or information offered on this Website, including any liability or expense arising from all claims, losses, damages (actual and consequential), suits, judgments, litigation costs and solicitors fees of every kind and nature incurred by you or any third parties through you.

## Intellectual property and copyrights

We hold the copyright to the content of this Website, including all uploaded files, layout design, data, graphics, articles, file content, codes, news, tutorials, videos, reviews, forum posts and databases contained on the Website or in connection with the Services. You must not use or replicate our copyright material other than as permitted by law.  Specifically, you must not use or replicate our copyright material for commercial purposes unless expressly agreed to by Us, in which case we may require you to sign a Licence Agreement.  

If you wish to use content, images or other of our intellectual property, you should submit your request to us at the following email address: 

**support@domainspot.net**

## Trademarks

The trademarks and logos contained on this Website are trademarks of DomainSpot.Net.  Use of these trademarks is strictly prohibited except with Our express, written consent.

## Links to external websites

This Website may contain links that direct you outside of this Website.  These links are provided for your convenience and are not an express or implied indication that we endorse or approve of the linked Website, it’s contents or any associated website, product or service.  We accept no liability for loss or damage arising out of or in connection to your use of these sites.  

You may link to our articles or home page.  However, you should not provide a link which suggests any association, approval or endorsement on our part in respect to your website, unless we have expressly agreed in writing.  We may withdraw our consent to you linking to our site at any time by notice to you.

## Limitation of Liability

We take no responsibility for the accuracy of any of the content or statements contained on this Website or in relation to our Services.  Statements made are by way of general comment only and you should satisfy yourself as to their accuracy.  Further, all of our Services are provided without a warranty with the exception of any warranties provided by law.  We are not liable for any damages whatsoever, incurred as a result of or relating to the use of the Website or our Services.

## Information Collection

Use of information you have provided us with, or that we have collected and retained relating to your use of the Website and/or our Services, is governed by our Privacy Policy.  By using this Website and the Services associated with this Website, you are agreeing to the Privacy Policy.  To view our Privacy Policy and read more about why we collect personal information from you and how we use that information, [click here](/privacy-policy).

## Confidentiality

All personal information you give us will be dealt with in a confidential manner in accordance with our Privacy Policy.  However, due to circumstances outside of our control, we cannot guarantee that all aspects of your use of this Website will be confidential due to the potential ability of third parties to intercept and access such information.

## Governing Law

These terms and conditions are governed by and construed in accordance with the laws of New South Whales, Australia.  Any disputes concerning this website are to be resolved by the courts having jurisdiction in New South Whales. 

We retain the right to bring proceedings against you for breach of these Terms and Conditions, in your country of residence or any other appropriate country or jurisdiction.

## Refunds

All services are provided “as is”, but with with the below clauses:

1. We do not refund you for Domain Name registration expiry & SSL expiry notification misses. This is because these services are provided free of cost and the credits you buy are only for the “ping” service to see if the website is up or not.
1. We try to run our services with 99% uptime. This means that we may not be able to serve you for 1% time in a month (approximately 7 hours a month) due to maintenance work on our servers. Since we promise you 99% uptime, any refunds of services due to us missing to notify you of your website being down, will be only provided if the outage at our end is more that 1% for that month. For the purpose of refunds, outages are calculated on a per month basis, since subscription fee is charged on a per month basis.
1. We do not refund any transaction cost or conversion fees.
1. We do not refund any additional fees charged by your financial institution.
1. If you have subscribed to the “ping” service, and we fail to notify you based on the alerts you’ve set and this accounts to more than 1% of outage time that month, then:
    
    You could notify by emailing us at support@domainspot.net within 24 hours of the immediate next subscription renewal date. In this case if we are able to confirm the issue at our end, we will refund an amount calculated by subscription_fee x ((hours_of_outage - 7)  / 100). For example, if your subscription fee is $10, and the total outage due to which we couldn’t notify that month was 14 hours, then we refund you: 10 x ((14-7)/100) = $0.07

    If you fail to cancel the subscription by emailing us at support@domainspot.net within 24 hours of immediate net subscription renewal date, no refund is provided.

    We, however, offer you a trial period of 7 days during which you are not charged and you are free to cancel the subscription upto 1 day before the trial period ends.

## Payment Processing

Stripe is our payment processor and all payments go through stripe. The online form you fill with your card information on our website is powered by Stripe and by using the form to make payments, you agree to Stripe terms and conditions in addition to the terms of service of DomainSpot.net You can find the terms of service and privacy policy for Stripe on their website at [https://stripe.com/au](https://stripe.com/au)

We do not store your credit card details on our website and only a Stipe subscription authorisation key is stored.

## Other Terms

If you wish to cancel your subscription, you may do so anytime from your account settings page (Click on your name on top Right of the website and click on profile) or by sending us an email at support@domainspot.net least 48 hours before the subscription renewal date. Any amount already charged will not be refunded but instead you will be able to use it till the end of the period for which the payment has been made.