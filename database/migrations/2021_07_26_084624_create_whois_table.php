<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhoisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whois', function (Blueprint $table) {
            $table->id();
            $table->text("whois");
            $table->foreignId("registrar_id")->constrained()->onDelete("RESTRICT");
            $table->timestamp("expires_at")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('whois', function (Blueprint $table) {
            $table->dropForeign(['registrar_id']);
        });


        Schema::dropIfExists('whois');
    }
}
