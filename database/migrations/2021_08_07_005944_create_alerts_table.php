<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alerts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->string('alertable_type'); // App/Model/Domain
            $table->bigInteger("alertable_id")->unsigned(); // domain.id
            $table->string("type"); // healthecheck.
            $table->string("frequency"); // healthcheck.<type>.frequency
            $table->integer("tolerance"); // healthcheck.<type>.tolerance
            $table->timestamp('last_checked')->nullable();
            $table->integer('port')->nullable();
            $table->integer("failures")->default(0);
            $table->boolean("alerted")->default(false);
            $table->timestamps();
        });

        /**
         * user_id 1
         * alertable_type App/Model/Domain
         * alertable_id 1
         * frequency 24 hours
         * type Domain Registration
         * tolerance 1
         * 
         * user_id 1
         * alertable_type App/Model/Certificate
         * alertable_id 1
         * type SSL on HTTPS
         * frequency 24 hours
         * tolerance 1
         * 
         * user_id 1
         * alertable_type App/Model/Domain
         * alertable_id 1
         * type Webserver Ping
         * frequency 30 seconds
         * tolerance 3
         */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alerts', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('alerts');
    }
}
