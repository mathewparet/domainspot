<?php

namespace Database\Seeders;

use App\Models\Ping;
use Illuminate\Database\Seeder;

class PingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ping::factory()
            // ->count(13392000)
            ->count(100000)
            ->create();
    }
}
